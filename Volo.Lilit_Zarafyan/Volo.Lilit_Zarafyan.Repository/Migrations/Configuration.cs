namespace Volo.Lilit_Zarafyan.Repository.Migrations
{
    using Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Volo.Lilit_Zarafyan.Repository.Entities.BankDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Volo.Lilit_Zarafyan.Repository.Entities.BankDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Banks.AddOrUpdate(d => d.VAT,
                     new Bank
                     {
                         BankName = "HSBC BANK CJSC",
                         Address = "66, Teryan St., Yerevan,",
                         VAT = "11122233"
                     },
                     new Bank
                     {
                         BankName = "VTB Bank CJSC",
                         Address = "46, Nalbandyan st., Yerevan",
                         VAT = "22233344"
                     },
                     new Bank
                     {
                         BankName = "Byblos Bank CJSC",
                         Address = "18/3 Amiryan St, Yerevan",
                         VAT = "33444555"
                     }
             );

        }
    }
}
