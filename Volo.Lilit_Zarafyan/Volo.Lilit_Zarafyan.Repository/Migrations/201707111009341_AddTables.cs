namespace Volo.Lilit_Zarafyan.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankCustomerDeposits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DepositId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        StartSum = c.Int(nullable: false),
                        ClosedDate = c.DateTime(),
                        Bank_Id = c.Int(),
                        Customer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Banks", t => t.Bank_Id)
                .ForeignKey("dbo.Customers", t => t.Customer_Id)
                .Index(t => t.Bank_Id)
                .Index(t => t.Customer_Id);
            
            CreateTable(
                "dbo.Banks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BankName = c.String(),
                        Address = c.String(),
                        VAT = c.String(),
                        DestroyedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LastName = c.String(),
                        PassportSerie = c.String(),
                        Address = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankCustomerDeposits", "Customer_Id", "dbo.Customers");
            DropForeignKey("dbo.BankCustomerDeposits", "Bank_Id", "dbo.Banks");
            DropIndex("dbo.BankCustomerDeposits", new[] { "Customer_Id" });
            DropIndex("dbo.BankCustomerDeposits", new[] { "Bank_Id" });
            DropTable("dbo.Customers");
            DropTable("dbo.Banks");
            DropTable("dbo.BankCustomerDeposits");
        }
    }
}
