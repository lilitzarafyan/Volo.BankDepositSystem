﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volo.Lilit_Zarafyan.Repository.DepositCategory
{
    public class DepositType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Percent { get; set; }
    }
}
