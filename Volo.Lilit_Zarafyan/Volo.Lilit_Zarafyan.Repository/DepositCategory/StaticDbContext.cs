﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Volo.Lilit_Zarafyan.Repository.DepositCategory
{
    static class StaticDbContext
    {
        static StaticDbContext()
        {
            InitDepositTypes();  
        }

        public static List<DepositType> DepositTypes { get; private set; }

        public static void Init() { }

        private static void InitDepositTypes()
        {
            DepositTypes = new List<DepositType>
            {
                new DepositType
            {
                Id = 1,
                Name = "Cumulative",
                Percent = 12
            },

             new DepositType
            {
                Id = 2,
                Name = "Family",
                Percent = 13
            },

             new DepositType
            {
                Id = 3,
                Name = "Saving",
                Percent = 9
            }
          };
        }
    }
}
