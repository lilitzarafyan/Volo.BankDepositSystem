﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Lilit_Zarafyan.Repository.DepositCategory;
using Volo.Lilit_Zarafyan.Repository.Entities;

namespace Volo.Lilit_Zarafyan.Repository.Services
{
    public interface IBankRepository : IDisposable
    {
        IEnumerable<TModel> SelectAll<TModel>() where TModel : class;
        IEnumerable<Bank> SelectActiveBanks();
        IEnumerable<Customer> SelectValidCustomers();
        IEnumerable<BankCustomerDeposit> SelectActiveDeposits();
        IEnumerable<BankCustomerDeposit> SelectClosedDeposits();
        List<DepositType> SelectDepositTypes();
        TModel Select<TModel>(int key) where TModel : class;
        bool Insert<TModel>(TModel entity) where TModel : class;
        bool InsertDeposit(BankCustomerDeposit deposit);
        bool DestroyBank(int id);
        bool CloseDeposit(int id);
        bool DeleteCustomer(int id);
        bool Update<TModel>(TModel entity) where TModel : class;
        void Save();

        bool Any<TModel>(Func<TModel, bool> predicate) where TModel : class;
        List<BankCustomerDeposit> Search(int? bankId, int? customerId, int? depositId, int? startSumFrom, int? startSumTo, DateTime? startDateFrom, DateTime? startDateTo);
        decimal CalculateProfit(int depositId, decimal money, int days);
    }
}
