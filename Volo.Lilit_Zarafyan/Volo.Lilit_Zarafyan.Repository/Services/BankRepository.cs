﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Lilit_Zarafyan.Repository.DepositCategory;
using Volo.Lilit_Zarafyan.Repository.Entities;

namespace Volo.Lilit_Zarafyan.Repository.Services
{
    public class BankRepository : IBankRepository
    {
        public BankRepository()
        {
            db = new BankDbContext();
        }

        private BankDbContext db;

        public IEnumerable<TModel> SelectAll<TModel>() where TModel : class
        {
            return db.Set<TModel>().ToList();
        }

        public IEnumerable<Bank> SelectActiveBanks()
        {
            return db.Banks.Where(bank => bank.DestroyedDate == null).ToList();
        }

        public IEnumerable<Customer> SelectValidCustomers()
        {
            return db.Customers.Where(c => c.IsDeleted == false).ToList();
        }

        public IEnumerable<BankCustomerDeposit> SelectActiveDeposits()
        {
            return db.BankCustomerDeposits.Where(item => item.ClosedDate == null).ToList();
        }

        public IEnumerable<BankCustomerDeposit> SelectClosedDeposits()
        {
            return db.BankCustomerDeposits.Where(item => item.ClosedDate != null).ToList();
        }

        public List<DepositType> SelectDepositTypes()
        {
            return StaticDbContext.DepositTypes;
        }

        public TModel Select<TModel>(int key) where TModel : class
        {
            return db.Set<TModel>().Find(key);
        }

        public bool Insert<TModel>(TModel entity) where TModel : class
        {
            if (entity == null)
                return false;

            try
            {
                db.Set<TModel>().Add(entity);
                return db.SaveChanges() >= 0;
            }
            catch
            {
                return false;
            }
        }

        public bool InsertDeposit(BankCustomerDeposit deposit)
        {
            deposit.StartDate = DateTime.Now;
            return Insert(deposit);
        } 

        public bool Any<TModel>(Func<TModel, bool> predicate) where TModel : class
        {
            return db.Set<TModel>().Any(predicate);
        }

        public bool Update<TModel>(TModel entity) where TModel : class
        {
            if (entity == null)
                return false;

            try
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() >= 0;
            }
            catch
            {
                return false;
            }
        }

        public void Save()
        {
            db.SaveChanges();
        }

        public bool DestroyBank(int id)
        {
            Bank bank = Select<Bank>(id);
            if (!Any<BankCustomerDeposit>(p => p.BankId == bank.Id))
            {
                bank.DestroyedDate = DateTime.Now;
                return db.SaveChanges() >= 0;
            }
            else
                return false;
        }

        public bool DeleteCustomer(int id)
        {
            Customer  customer= Select<Customer>(id);
            if (!Any<BankCustomerDeposit>(p => p.CustomerId == customer.Id))
            {
                customer.IsDeleted = true;
                return db.SaveChanges() >= 0;
            }
            else
                return false;
        }

        public bool CloseDeposit(int id)
        {
            BankCustomerDeposit deposit = Select<BankCustomerDeposit>(id);
            deposit.ClosedDate = DateTime.Now;
            int periodInDays = (DateTime.Now - deposit.StartDate).Days;
            deposit.TotalProfitSum = CalculateProfit(deposit.DepositId, deposit.StartSum, periodInDays);
            return db.SaveChanges() >= 0;
        }

        public List<BankCustomerDeposit> Search(int? bankId, int? customerId, int? depositId, int? startSumFrom, int? startSumTo, DateTime? startDateFrom, DateTime? startDateTo)
        {
            var sb = new StringBuilder();
            sb.Append("Select*FROM BankCustomerDeposits WHERE ClosedDate is null AND");

            if (bankId != null)
                sb.Append(" BankId={0} AND");

            if (customerId != null)
                sb.Append(" CustomerId={1} AND");

            if (depositId != null)
            {
                sb.Append(" DepositId={2} AND");
            }
            if (startSumFrom != null)
            {
                sb.Append(" StartSum > {3} AND");
            };
            if (startSumTo != null)
            {
                sb.Append(" StartSum < {4} AND");
            }
            if (startDateFrom != null)
            {
                sb.Append(" StartDate > {5} AND");
            };
            if (startDateTo != null)
            {
                sb.Append(" StartDate < {6} AND");
            }
            string search = sb.ToString().Remove(sb.Length - 3, 3);

            return db.BankCustomerDeposits.SqlQuery(search, bankId, customerId, depositId, startSumFrom, startSumTo, startDateFrom, startDateTo).ToList();
        }

        public decimal CalculateProfit(int depositId, decimal money, int days)
        {
            DepositType deposit = StaticDbContext.DepositTypes.Single(d=>d.Id == depositId);
            return money+ ((money * (decimal)deposit.Percent / 100)/365) * days;
        }

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }


        ~BankRepository()
        {
            Dispose(false);
        }

        #endregion
    }
}