using System;

namespace Volo.Lilit_Zarafyan.Repository.Entities
{ 
    
    public partial class BankCustomerDeposit
    {
        public int Id { get; set; }
        public int BankId { get; set; }
        public int CustomerId { get; set; }
        public int DepositId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal StartSum { get; set; }
        public decimal? TotalProfitSum { get; set; }
        public DateTime? ClosedDate { get; set; }
    
        public virtual Bank Bank { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
