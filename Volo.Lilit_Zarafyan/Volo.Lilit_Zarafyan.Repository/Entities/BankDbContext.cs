﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.Repository.Migrations;

namespace Volo.Lilit_Zarafyan.Repository.Entities
{

    public class BankDbContext : DbContext
    {
        public BankDbContext()
            : base("BankDBEntities")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BankDbContext,Configuration>());
        }

        public static BankDbContext Create()
        {
            return new BankDbContext();
        }

        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankCustomerDeposit> BankCustomerDeposits { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }

}
