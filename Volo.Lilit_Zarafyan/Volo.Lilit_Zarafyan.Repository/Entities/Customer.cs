﻿using System;
using System.Collections.Generic;

namespace Volo.Lilit_Zarafyan.Repository.Entities
{
    public class Customer
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PassportSerie { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsDeleted { get; set; }

        public virtual ICollection<BankCustomerDeposit> BankCustomerDeposits { get; set; }

    }
}