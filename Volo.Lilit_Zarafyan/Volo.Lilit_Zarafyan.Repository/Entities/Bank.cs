using System;
using System.Collections.Generic;
namespace Volo.Lilit_Zarafyan.Repository.Entities
{
    public class Bank
    {    
        public int Id { get; set; }
        public string BankName { get; set; }
        public string Address { get; set; }
        public string VAT { get; set; }
        public DateTime? DestroyedDate { get; set; }

        public virtual ICollection<BankCustomerDeposit> BankCustomerDeposits { get; set; }
    }
}
