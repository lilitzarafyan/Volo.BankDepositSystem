﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Volo.Lilit_Zarafyan.MVC.ViewModel
{
    public class CustomerViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public string Name { get; set; }

        [Required(ErrorMessage = "It is required field")]
        [Display(Name="Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "It is required field")]
        [Display(Name = "Passport Serie")]
        public string PassportSerie { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public string Address { get; set; }

        [Required(ErrorMessage = "It is required field")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "It is required field")]
        [StringLength(12)]
        public string Phone { get; set; }

        [DataType(DataType.Date)]
        public string Birthday { get; set; }

        [Display(Name="Full Name")]
        public string FullName { get { return Name + " " + LastName; } }
        public string CustomerInfo { get { return "FullName: " + FullName + " Address: " + Address + " Email: " + Email+" Phone "+Phone; } }

    }
}