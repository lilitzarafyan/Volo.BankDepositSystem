﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Volo.Lilit_Zarafyan.MVC.ViewModel
{
    public class DepositTypeViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public string Name { get; set; }

        public double Percent { get; set; }

        public string DepTypeInfo { get { return "Name: " + Name + " Percent: " + Percent + "%"; } }
    }
}