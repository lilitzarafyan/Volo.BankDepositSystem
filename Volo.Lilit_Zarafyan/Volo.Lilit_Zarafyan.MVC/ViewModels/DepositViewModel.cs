﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.DepositCategory;

namespace Volo.Lilit_Zarafyan.MVC.ViewModel
{
    public class DepositViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public int BankId { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public int CustomerId { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public int DepositId { get; set; }

        [Display(Name ="Start Date")]
        public System.DateTime StartDate { get; set; }

        [Required(ErrorMessage = "It is required field")]
        [Display(Name = "Estimated End Date")]
        [DataType(DataType.Date)]
        public System.DateTime EndDate { get; set; }

        [Required(ErrorMessage = "It is required field")]
        [Display(Name = "Start Sum(AMD)")]
        public decimal StartSum { get; set; }

        [Display(Name = "Total Profit Sum(AMD)")]
        public Nullable<decimal> TotalProfitSum { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> ClosedDate { get; set; }

        public BankViewModel Bank { get; set; }

        public IEnumerable<BankViewModel> Banks { get; set; }

        public CustomerViewModel Customer { get; set; }

        public IEnumerable<CustomerViewModel> Customers { get; set; }

        [Display(Name ="Deposit Type")]
        public DepositTypeViewModel DepType { get; set; }

        public List<DepositTypeViewModel> DepTypes { get; set; }

    }
}