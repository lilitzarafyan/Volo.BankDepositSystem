﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Volo.Lilit_Zarafyan.MVC.ViewModel
{
    public class BankViewModel
    {
        [HiddenInput]
        public int Id { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public string Address { get; set; }

        [Required(ErrorMessage = "It is required field")]
        public string VAT { get; set; }

        public Nullable<System.DateTime> DestroyedDate { get; set; }

        public string BankInfo { get { return "Name: " + BankName + " Address: " + Address + " VAT: " + VAT; } }

    }
}