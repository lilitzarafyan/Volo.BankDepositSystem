﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Volo.Lilit_Zarafyan.MVC.ViewModel
{
    public class SearchViewModel
    {
        public int BankId { get; set; }
        public BankViewModel Bank { get; set; }
        public IEnumerable<BankViewModel> Banks { get; set; }

        public int CustomerId { get; set; }
        public CustomerViewModel Customer { get; set; }
        public IEnumerable<CustomerViewModel> Customers { get; set; }

        public int DepositId { get; set; }

        [Display(Name = "Deposit Type")]
        public DepositTypeViewModel DepType { get; set; }

        public List<DepositTypeViewModel> DepTypes { get; set; }

        [Display(Name = "Start Deposit Sum From")]
        public int StartSumFrom { get; set; }

        [Display(Name = "Start Deposit Sum To")]
        public int StartSumTo { get; set; }

        [Display(Name = "Start Deposit Date From")]
        public System.DateTime StartDateFrom { get; set; }

        [Display(Name = "Start Deposit Date To")]
        public System.DateTime StartDateTo { get; set; }
    }
}