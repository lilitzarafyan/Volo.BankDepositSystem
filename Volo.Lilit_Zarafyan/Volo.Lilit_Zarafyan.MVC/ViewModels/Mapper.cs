﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Volo.Lilit_Zarafyan.Repository.DepositCategory;
using Volo.Lilit_Zarafyan.Repository.Entities;

namespace Volo.Lilit_Zarafyan.MVC.ViewModel
{
    public static class Mapper
    {
        #region ToViewModel

        public static BankViewModel ToViewModel(Bank bank)
        {
            var viewModel = new BankViewModel
            {
                Id = bank.Id,
                BankName = bank.BankName,
                Address = bank.Address,
                VAT = bank.VAT,
                DestroyedDate = bank.DestroyedDate
            };
            return viewModel;
        }

        public static CustomerViewModel ToViewModel(Customer customer)
        {
            var viewModel = new CustomerViewModel
            {
                Id = customer.Id,
                Name = customer.Name,
                LastName = customer.LastName,
                PassportSerie=customer.PassportSerie,
                Address = customer.Address,
                Email = customer.Email,
                Phone = customer.Phone,
            };
            return viewModel;
        }

        public static DepositTypeViewModel ToViewModel(DepositType deptype)
        {
            var viewModel = new DepositTypeViewModel
            {
                Id = deptype.Id,
                Name = deptype.Name,
                Percent = deptype.Percent,
            };
            return viewModel;
        }

        public static DepositViewModel ToViewModel(BankCustomerDeposit deposit, IEnumerable<DepositType> depTypes)
        {
            var depositVM = new DepositViewModel();

            depositVM.Id = deposit.Id;
            depositVM.BankId = deposit.BankId;
            depositVM.Bank = ToViewModel(deposit.Bank);
            depositVM.CustomerId = deposit.CustomerId;
            depositVM.Customer = ToViewModel(deposit.Customer);
            depositVM.DepositId = deposit.DepositId;
            foreach (var dtype in depTypes)
            {
                if (depositVM.DepositId == dtype.Id)
                {
                    depositVM.DepType = ToViewModel(dtype);
                    break;
                }
            }
            depositVM.StartDate = deposit.StartDate;
            depositVM.EndDate = deposit.EndDate;
            depositVM.StartSum = deposit.StartSum;
            depositVM.TotalProfitSum = deposit.TotalProfitSum;
            depositVM.ClosedDate = deposit.ClosedDate;

            return depositVM;
        }
        public static SearchViewModel ToSearchViewModel(BankCustomerDeposit deposit, IEnumerable<DepositType> depTypes)
        {
            SearchViewModel searchVM = new SearchViewModel();
            searchVM.BankId = deposit.Bank.Id;
            searchVM.Bank = ToViewModel(deposit.Bank);
            searchVM.CustomerId = deposit.Customer.Id;
            searchVM.Customer = ToViewModel(deposit.Customer);

            searchVM.DepositId = deposit.DepositId;
            foreach (var dtype in depTypes)
            {
                if (searchVM.DepositId == dtype.Id)
                    searchVM.DepType = ToViewModel(dtype);
            }

            return searchVM;
        }
        #endregion

        #region ToModel

        public static Bank ToModel(BankViewModel bankVM)
        {
            var model = new Bank
            {
                Id = bankVM.Id,
                BankName = bankVM.BankName,
                Address = bankVM.Address,
                VAT = bankVM.VAT,
                DestroyedDate = bankVM.DestroyedDate
            };
            return model;
        }

        public static Customer ToModel(CustomerViewModel customer)
        {
            var model = new Customer
            {
                Id = customer.Id,
                Name = customer.Name,
                LastName = customer.LastName,
                PassportSerie=customer.PassportSerie,
                Address = customer.Address,
                Email= customer.Email,
                Phone = customer.Phone,
            };
            return model;
        }

        public static BankCustomerDeposit ToModel(DepositViewModel depositVM)
        {
            var model = new BankCustomerDeposit
            {
                Id = depositVM.Id,
                BankId = depositVM.BankId,
                CustomerId = depositVM.CustomerId,
                DepositId = depositVM.DepositId,
                StartDate = depositVM.StartDate,
                EndDate = depositVM.EndDate,
                StartSum = depositVM.StartSum,
                TotalProfitSum = depositVM.TotalProfitSum,
                ClosedDate = depositVM.ClosedDate
            };
            return model;
        }

        #endregion

        #region ToViewModelCollection
        public static List<DepositTypeViewModel> ToViewModelCollection(List<DepositType> deptypes)
        {
            var viewModels = new List<DepositTypeViewModel>();
            foreach (var model in deptypes)
            {
                var viewModel = ToViewModel(model);
                viewModels.Add(viewModel);
            }
            return viewModels;
        }

        public static IEnumerable<BankViewModel> ToViewModelCollection(IEnumerable<Bank> banks)
        {
            return banks.Select(ToViewModel);
        }

        public static IEnumerable<CustomerViewModel> ToViewModelCollection(IEnumerable<Customer> customers)
        {
            return customers.Select(ToViewModel);
        }

        public static IEnumerable<DepositViewModel> ToViewModelCollection(IEnumerable<BankCustomerDeposit> deposits, IEnumerable<DepositType> deptypes)
        {
            var viewModels = new List<DepositViewModel>();
            foreach (var model in deposits)
            {
                var viewModel = ToViewModel(model, deptypes);
                viewModels.Add(viewModel);
            }
            return viewModels;
        }

        public static IEnumerable<SearchViewModel> ToSearchViewModelCollection(IEnumerable<BankCustomerDeposit> deposits, IEnumerable<DepositType> depTypes)
        {
            var viewModels = new List<SearchViewModel>();
            foreach (var model in deposits)
            {
                var viewModel = ToSearchViewModel(model, depTypes);
                viewModels.Add(viewModel);
            }
            return viewModels;
        }
        #endregion


        ////public static TViewModel ToViewModel<TViewModel>(object model)
        ////    where TViewModel : class, new()
        ////{
        ////    var viewModel = new TViewModel();

        ////    var modelType = model.GetType();
        ////    foreach (var pi in viewModel.GetType().GetProperties())
        ////    {
        ////        var piModel = modelType.GetProperty(pi.Name);
        ////        if (piModel == null)
        ////            continue;

        ////        var value = piModel.GetValue(model);
        ////        pi.SetValue(viewModel, value);
        ////    }

        ////    return viewModel;
        ////}
    }
}