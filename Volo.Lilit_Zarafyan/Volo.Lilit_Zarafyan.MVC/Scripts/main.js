﻿(function ($) {

    $(function () {
        $('.date-end input').datepicker({
            format: 'mm-dd-yyyy',
            autoclose: true
        });
        $('.date-start input').datepicker({
            format: 'mm-dd-yyyy',
            autoclose: true
        });
    });
 
})(jQuery);