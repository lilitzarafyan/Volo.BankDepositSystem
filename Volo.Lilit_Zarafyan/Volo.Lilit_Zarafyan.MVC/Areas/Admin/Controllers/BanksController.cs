﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.MVC.ViewModel;

namespace Volo.Lilit_Zarafyan.MVC.Areas.Admin.Controllers
{
    public class BanksController : BaseController
    {
        // GET: Admin/Banks
        public ActionResult Index()
        {
            IEnumerable<BankViewModel> banksVM = Mapper.ToViewModelCollection(_repository.SelectActiveBanks());

            return View(banksVM);
        }

        // GET: Admin/Banks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BankViewModel bankVM = Mapper.ToViewModel(_repository.Select<Bank>(id.Value));
            if (bankVM == null)
            {
                return HttpNotFound();
            }
            return View(bankVM);
        }

        // GET: Admin/Banks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Banks/Create
        [HttpPost]
        public ActionResult Create(BankViewModel bankVM)
        {
            if (ModelState.IsValid)
            {
                Bank bank = Mapper.ToModel(bankVM);
                if (!_repository.Any<Bank>(p => p.VAT == bank.VAT))
                {
                    _repository.Insert(bank);
                    return PartialView("_CreateSuccess");
                }
                else
                    return PartialView("_CreateError");
            }
            return View(bankVM);
        }

        // GET: Admin/Banks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankViewModel bank = Mapper.ToViewModel(_repository.Select<Bank>(id.Value));
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // POST: Admin/Banks/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,BankName,Address,VAT,DestroyedDate")] BankViewModel bankVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Bank bank = Mapper.ToModel(bankVM);
                    _repository.Update(bank);
                    return RedirectToAction("Index");
                }
                return View(bankVM);
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Banks/Delete/5
        public ActionResult Destroy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankViewModel bankVM = Mapper.ToViewModel(_repository.Select<Bank>(id.Value));
            if (bankVM == null)
            {
                return HttpNotFound();
            }
            return View(bankVM);
        }

        // POST: Admin/Banks/Delete/5
        [HttpPost]
        public ActionResult Destroy(int id)
        {
            try
            {
                if (_repository.DestroyBank(id))
                    return PartialView("_DestroySuccess");
                else
                    return PartialView("_DestroyError");
            }
            catch
            {
                return View();
            }
        }
    }
}
