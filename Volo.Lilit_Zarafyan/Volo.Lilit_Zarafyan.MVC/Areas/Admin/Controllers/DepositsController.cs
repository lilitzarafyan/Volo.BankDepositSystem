﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.DepositCategory;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.MVC.ViewModel;

namespace Volo.Lilit_Zarafyan.MVC.Areas.Admin.Controllers
{
    public class DepositsController : BaseController
    {

        // GET: Admin/Deposits
        public ActionResult Index()
        {
            IEnumerable<DepositViewModel> depositsVM = Mapper.ToViewModelCollection(_repository.SelectActiveDeposits(), _repository.SelectDepositTypes());
            return View(depositsVM);
        }

        // GET: Admin/Deposits
        public ActionResult ClosedDepositsList()
        {
            IEnumerable<DepositViewModel> depositsVM = Mapper.ToViewModelCollection(_repository.SelectClosedDeposits(), _repository.SelectDepositTypes());
            return View(depositsVM);
        }

        // GET: Admin/Deposits/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            DepositViewModel depositVM = Mapper.ToViewModel(_repository.Select<BankCustomerDeposit>(id.Value), _repository.SelectDepositTypes());
            if (depositVM == null)
            {
                return HttpNotFound();
            }
            return View(depositVM);
        }

        // GET: Admin/Deposits/Create
        public ActionResult Create(int? id)
        {
            DepositViewModel depositVM = new DepositViewModel();
            IEnumerable<BankViewModel> banksVM = Mapper.ToViewModelCollection(_repository.SelectActiveBanks());
            IEnumerable<CustomerViewModel> customersVM = Mapper.ToViewModelCollection(_repository.SelectValidCustomers());
            List<DepositTypeViewModel> dtypesVM = Mapper.ToViewModelCollection(_repository.SelectDepositTypes());
            depositVM.Banks = banksVM;
            depositVM.Customers = customersVM;
            depositVM.DepTypes = dtypesVM;
            ViewBag.DepositId = id;

            return View(depositVM);
        }

        // POST: Admin/Deposits/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "BankId, CustomerId,DepositId,StartDate,EndDate,StartSum")]DepositViewModel depositVM)
        {
            if (ModelState.IsValid)
            {
                BankCustomerDeposit deposit = Mapper.ToModel(depositVM);
                if (_repository.InsertDeposit(deposit))
                    return View("_CreateSuccess");
            }
            return View();
        }
        // GET: Admin/Deposits/Delete/5
        public ActionResult CloseDeposit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DepositViewModel depositVM = Mapper.ToViewModel(_repository.Select<BankCustomerDeposit>(id.Value), _repository.SelectDepositTypes());
             if (depositVM == null)
            {
                return HttpNotFound();
            }
            return View(depositVM);
        }

        // POST: Admin/Deposits/Delete/5
        [HttpPost]
        public ActionResult CloseDeposit(int id)
        {
            try
            {
                if (_repository.CloseDeposit(id))
                    return View("_ClosedSuccess");
                return View("Index");
            }
            catch
            {
                return View();
            }
        }


        public string CalculateProfit(int DepositId, decimal money, int periodInMonth)
        {
            return String.Format("{0:0.00}"+"AMD", _repository.CalculateProfit(DepositId, money, periodInMonth*30));
        }

    }
}
