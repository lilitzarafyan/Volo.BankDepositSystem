﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.MVC.ViewModel;
using Volo.Lilit_Zarafyan.Repository.Entities;

namespace Volo.Lilit_Zarafyan.MVC.Areas.Admin.Controllers
{
    public class SearchController : BaseController
    {
        // GET: Admin/Search
        public ActionResult Index()
        {
            SearchViewModel searchVM = new SearchViewModel();
            IEnumerable<BankViewModel> banksVM = Mapper.ToViewModelCollection(_repository.SelectActiveBanks());
            IEnumerable<CustomerViewModel> customersVM = Mapper.ToViewModelCollection(_repository.SelectAll<Customer>());
            List<DepositTypeViewModel> dtypesVM = Mapper.ToViewModelCollection(_repository.SelectDepositTypes());
            searchVM.Banks = banksVM;
            searchVM.Customers = customersVM;
            searchVM.DepTypes = dtypesVM;

            return View(searchVM);
        }

        // GET: Admin/Search/SearchResult/5
        public ActionResult SearchResult(int? BankId, int? CustomerId, int? DepositId, int? StartSumFrom, int? StartSumTo, DateTime? StartDateFrom, DateTime? StartDateTo)
        {
            if (ModelState.IsValid)
            {
                List<BankCustomerDeposit> searchResult = _repository.Search(BankId, CustomerId, DepositId, StartSumFrom, StartSumTo, StartDateFrom, StartDateTo);

                IEnumerable<SearchViewModel> searchsVM = Mapper.ToSearchViewModelCollection(searchResult, _repository.SelectDepositTypes());
                return View(searchsVM);
            }
            return View();
        }
    }
}
