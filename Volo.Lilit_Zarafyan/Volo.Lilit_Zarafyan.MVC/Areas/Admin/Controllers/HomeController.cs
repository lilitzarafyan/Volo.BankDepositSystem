﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.MVC.ViewModel;

namespace Volo.Lilit_Zarafyan.MVC.Areas.Admin.Controllers
{
    public class HomeController:BaseController
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}