﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.MVC.ViewModel;

namespace Volo.Lilit_Zarafyan.MVC.Areas.Admin.Controllers
{
    public class CustomersController : BaseController
    {
        // GET: Admin/Customers
        public ActionResult Index()
        {
            IEnumerable<CustomerViewModel> customers = Mapper.ToViewModelCollection(_repository.SelectAll<Customer>());
            return View(customers);
        }

        // GET: Admin/Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CustomerViewModel customerVM = Mapper.ToViewModel(_repository.Select<Customer>(id.Value));
            if (customerVM == null)
            {
                return HttpNotFound();
            }
            return View(customerVM);
        }

        // GET: Admin/Customers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Customers/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,Name,LastName,PassportSerie,Address,Email,Phone")] CustomerViewModel customerVM)
        {
            if (ModelState.IsValid)
            {
                Customer customer = Mapper.ToModel(customerVM);
                if (!_repository.Any<Customer>(p => p.PassportSerie == customer.PassportSerie))
                {
                    _repository.Insert(customer);
                    return View("_CreateSuccess");
                }
                else
                    return View("_CreateError");
            }
            return View(customerVM);
        }

        // GET: Admin/Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerViewModel customerVM = Mapper.ToViewModel(_repository.Select<Customer>(id.Value));
            if (customerVM == null)
            {
                return HttpNotFound();
            }
            return View(customerVM);
        }

        // POST: Admin/Customers/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include = "Id,Name,LastName,PassportSerie,Address,Email,Phone,Birthday")] CustomerViewModel customerVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Customer customer = Mapper.ToModel(customerVM);
                    _repository.Update(customer);
                    return RedirectToAction("Index");
                }
                return View(customerVM);
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/Customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomerViewModel customerVM = Mapper.ToViewModel(_repository.Select<Customer>(id.Value));
            if (customerVM == null)
            {
                return HttpNotFound();
            }
            return View(customerVM);
        }

        // POST: Admin/Customers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                if (_repository.DeleteCustomer(id))
                    return PartialView("_DeleteSuccess");
                else
                    return PartialView("_DeleteError");
            }
            catch
            {
                return View();
            }
        }
    }
}
