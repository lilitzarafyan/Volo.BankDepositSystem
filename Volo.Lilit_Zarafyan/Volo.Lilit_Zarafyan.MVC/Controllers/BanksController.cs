﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.MVC.ViewModel;
using Volo.Lilit_Zarafyan.MVC.Areas.Admin.Controllers;

namespace Volo.Lilit_Zarafyan.MVC.Controllers
{
    public class BanksController : BaseController
    {
        // GET: Banks
        public ActionResult Index()
        {
            IEnumerable<BankViewModel> banksVM = Mapper.ToViewModelCollection(_repository.SelectActiveBanks());

            return View(banksVM);
        }

        // GET: Banks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            BankViewModel bankVM = Mapper.ToViewModel(_repository.Select<Bank>(id.Value));
            if (bankVM == null)
            {
                return HttpNotFound();
            }
            return View(bankVM);
        }
    }
}
