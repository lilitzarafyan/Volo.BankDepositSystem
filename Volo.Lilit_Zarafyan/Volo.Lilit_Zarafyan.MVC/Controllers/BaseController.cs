﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.Services;

namespace Volo.Lilit_Zarafyan.MVC.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IBankRepository _repository;

        protected BaseController()
            : this(new BankRepository())
        { }

        protected BaseController(IBankRepository repository)
        {
            _repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            if (_repository != null)
            {
                _repository.Dispose();
                _repository = null;
            }
            base.Dispose(disposing);
        }
    }
}