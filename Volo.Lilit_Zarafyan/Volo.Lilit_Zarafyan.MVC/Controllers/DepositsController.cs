﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Volo.Lilit_Zarafyan.Repository.DepositCategory;
using Volo.Lilit_Zarafyan.Repository.Entities;
using Volo.Lilit_Zarafyan.MVC.ViewModel;

namespace Volo.Lilit_Zarafyan.MVC.Controllers
{
    public class DepositsController : BaseController
    {

        // GET: Deposits/Create
        public ActionResult Create(int? id)
        {
            DepositViewModel depositVM = new DepositViewModel();
            IEnumerable<BankViewModel> banksVM = Mapper.ToViewModelCollection(_repository.SelectActiveBanks());
            IEnumerable<CustomerViewModel> customersVM = Mapper.ToViewModelCollection(_repository.SelectValidCustomers());
            List<DepositTypeViewModel> dtypesVM = Mapper.ToViewModelCollection(_repository.SelectDepositTypes());
            depositVM.Banks = banksVM;
            depositVM.Customers = customersVM;
            depositVM.DepTypes = dtypesVM;
            ViewBag.DepositId = id;

            return View(depositVM);
        }

        // POST: Deposits/Create
        [HttpPost]
        public ActionResult Create([Bind(Include = "BankId, CustomerId,DepositId,StartDate,EndDate,StartSum")]DepositViewModel depositVM)
        {
            if (ModelState.IsValid)
            {
                BankCustomerDeposit deposit = Mapper.ToModel(depositVM);
                if (_repository.InsertDeposit(deposit))
                    return View("_CreateSuccess");
            }
            return View();
        }

        public string CalculateProfit(int DepositId, decimal money, int periodInMonth)
        {
            return String.Format("{0:0.00}", _repository.CalculateProfit(DepositId, money, periodInMonth*30));
        }

    }
}
